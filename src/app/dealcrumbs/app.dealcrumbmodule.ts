import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { DealCrumpHome } from './homepage/app.homepage';
import { Header } from './homepage/header/homepage.header';
import { MaterialModule } from '@angular/material';
import { Tabs } from './homepage/tabs/app.tabs';
import { Deals } from './homepage/deals/app.deal';
import { Menu } from './homepage/menu/app.menu';
import { RetailPartners } from './homepage/stores/app.retailpartners';
import { CommonModule } from '@angular/common';
import { Banner } from './homepage/banner/app.banner';
import { DealGrid } from './homepage/feautureddeals/app.dealgrid';
import { FdealsFooter } from './homepage/feautureddeals/app.featuredeals.footer';
import { MissedDeals } from './homepage/misseddeals/app.misseddeals';
import {Test } from './test';

import { ProductDetailComponent } from './product-detail/product-detail/product-detail.component';
import { ProductInfoDetailInfoComponent } from './product-detail/product-info-detail-info/product-info-detail-info.component';
import { ProductInfoPricingHistoryComponent } from './product-detail/product-info-pricing-history/product-info-pricing-history.component';
import { ProductInfoCouponsOtherDealsComponent } from './product-detail/product-info-coupons-other-deals/product-info-coupons-other-deals.component';

import { APP_ROUTES } from './app.dealcrumb.routing.module';
import { RouterModule } from '@angular/router';
import { NgxChartsModule } from '@swimlane/ngx-charts';

@NgModule({
  imports: [MaterialModule, CommonModule, RouterModule.forRoot(APP_ROUTES), NgxChartsModule],
  exports: [DealCrumpHome, Header, Tabs, Deals, Menu, RetailPartners, Banner, DealGrid, FdealsFooter, MissedDeals, ProductDetailComponent,
    ProductInfoDetailInfoComponent, ProductInfoPricingHistoryComponent, ProductInfoCouponsOtherDealsComponent],
  declarations: [DealCrumpHome, Header, Tabs, Deals, Menu, RetailPartners, Banner, DealGrid, FdealsFooter, MissedDeals, ProductDetailComponent,
    ProductInfoDetailInfoComponent, ProductInfoPricingHistoryComponent, ProductInfoCouponsOtherDealsComponent],
  providers: [],
  entryComponents: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DealCrumpModule {

}
