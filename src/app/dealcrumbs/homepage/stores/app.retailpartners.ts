import {Component } from '@angular/core';
import { StoreService } from './app.storeservice';

@Component({
  selector: 'retail-partners',
  templateUrl: './app.retailstores.html',

  styleUrls: ['./app.retailstore.scss'],
  providers: [StoreService]

})
export class RetailPartners
{

storeService: StoreService;
stores: any;
constructor(storeService: StoreService)
{
  this.storeService = storeService;
}
ngOnInit()
{
   this.storeService.getRetailPartners().subscribe(retailPartners => { this.stores = retailPartners; });
}

}
