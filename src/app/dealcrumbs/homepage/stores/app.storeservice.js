"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Rx_1 = require("rxjs/Rx");
var StoreService = (function () {
    function StoreService() {
    }
    StoreService.prototype.getRetailPartners = function () {
        var retailPartners = [{ storename: '', storeimageUrl: './images/AmericanEagle-small.png', storeDetails: '', offer: '' },
            { storename: '', storeimageUrl: './images/bath-amp-body-works-small.png', storeDetails: '', offer: '' },
            { storename: '', storeimageUrl: './images/dominos-small.png', storeDetails: '', offer: '' },
            { storename: '', storeimageUrl: './images/footlocker-small.png', storeDetails: '', offer: '' },
            { storename: '', storeimageUrl: './images/oldnavy-small.png', storeDetails: '', offer: '' },
            { storename: '', storeimageUrl: './images/papa-small.png', storeDetails: '', offer: '' }
        ];
        return Rx_1.Observable.of(retailPartners);
    };
    return StoreService;
}());
exports.StoreService = StoreService;
//# sourceMappingURL=app.storeservice.js.map