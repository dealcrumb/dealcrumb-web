import { Observable } from 'rxjs/Rx';

export class StoreService {
  getRetailPartners(): Observable<any>
  {
    let retailPartners=[{storename:'',storeimageUrl:'./images/AmericanEagle-small.png',storeDetails:'',offer:''},
    {storename:'',storeimageUrl:'./images/bath-amp-body-works-small.png',storeDetails:'',offer:''},
    {storename:'',storeimageUrl:'./images/dominos-small.png',storeDetails:'',offer:''},
    {storename:'',storeimageUrl:'./images/footlocker-small.png',storeDetails:'',offer:''},
    {storename:'',storeimageUrl:'./images/oldnavy-small.png',storeDetails:'',offer:''},
    {storename:'',storeimageUrl:'./images/papa-small.png',storeDetails:'',offer:''}

    ];


    return Observable.of(retailPartners);
  }

}
