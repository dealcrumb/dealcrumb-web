"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_storeservice_1 = require("./app.storeservice");
var RetailPartners = (function () {
    function RetailPartners(storeService) {
        this.storeService = storeService;
    }
    RetailPartners.prototype.ngOnInit = function () {
        var _this = this;
        this.storeService.getRetailPartners().subscribe(function (retailPartners) { _this.stores = retailPartners; });
    };
    RetailPartners = __decorate([
        core_1.Component({
            selector: 'retail-partners',
            templateUrl: './app.retailstores.html',
            styleUrls: ['./app.retailstore.scss'],
            providers: [app_storeservice_1.StoreService]
        }),
        __metadata("design:paramtypes", [app_storeservice_1.StoreService])
    ], RetailPartners);
    return RetailPartners;
}());
exports.RetailPartners = RetailPartners;
//# sourceMappingURL=app.retailpartners.js.map