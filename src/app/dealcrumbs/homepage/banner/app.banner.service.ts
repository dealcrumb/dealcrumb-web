import { Observable } from 'rxjs/Rx';
export class BannerService
{
  banner: [{title: '', subtitle: '', imageUrl: '', color: ''},
  {title: '', subtitle: '', imageUrl: '', color: ''},
  {title: '', subtitle: '', imageUrl: '', color: ''}];

  getBanners(): Observable<[{}]>
  {
    return Observable.of(this.banner);
  }

}
