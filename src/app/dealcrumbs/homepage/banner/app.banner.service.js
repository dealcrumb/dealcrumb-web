"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Rx_1 = require("rxjs/Rx");
var BannerService = (function () {
    function BannerService() {
    }
    BannerService.prototype.getBanners = function () {
        return Rx_1.Observable.of(this.banner);
    };
    return BannerService;
}());
exports.BannerService = BannerService;
//# sourceMappingURL=app.banner.service.js.map