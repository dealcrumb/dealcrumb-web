import { Component } from '@angular/core';
import { BannerService } from './app.banner.service' ;

@Component({
  selector: 'banner',
  templateUrl: './app.banner.html',
  styleUrls: ['./app.banner.scss'],
  providers: [ BannerService ]
})
export class Banner
{
  bannerService: BannerService;
  banners:[{}];
  constructor(bannerService: BannerService)
  {
    this.bannerService = bannerService;
  }
  ngOnInit()
  {
    this.bannerService.getBanners().subscribe(banners => {this.banners = banners; } );
  }

}
