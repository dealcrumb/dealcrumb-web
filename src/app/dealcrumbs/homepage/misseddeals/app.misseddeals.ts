
import {Component} from '@angular/core';
import  { MissedDealsService } from './../misseddeals/app.service.misseddeals';

@Component({
  selector: 'missed-deals',
  templateUrl: './app.misseddeals.html',
  styleUrls: ['./app.misseddeals.scss'],
  providers: [MissedDealsService]
})
export class MissedDeals
{
   missedDealsService:MissedDealsService;
   mdeals: [{}];
   constructor(missedDealsService: MissedDealsService)
   {
    this.missedDealsService = missedDealsService;
   }
   ngOnInit()
   {
    this.missedDealsService.getMissedDeals().subscribe(mdeals=>{this.mdeals=mdeals});
   }
}
