"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Rx_1 = require("rxjs/Rx");
var MissedDealsService = (function () {
    function MissedDealsService() {
    }
    MissedDealsService.prototype.getMissedDeals = function () {
        var missedDeals = [{ discountPrice: '190.0', actualPrice: '321.0', img: './images/fd-beats.png', ItemName: 'Studio Beats By Dre',
                dealvalue: '10', cashBackName: ' Amazon Cash Back', shareBonus: '1%' },
            { discountPrice: '13.0', actualPrice: '19.0', img: './images/fd-chair.png', ItemName: 'Camping Chair',
                dealvalue: '10', cashBackName: 'REI Cash Back', shareBonus: '1%' },
            { discountPrice: '10.0', actualPrice: '20.0', img: './images/fd-dell.png', ItemName: 'Dell XPS 5000',
                dealvalue: '10', cashBackName: 'Dell Cash Back', shareBonus: '1%' },
            { discountPrice: '190.0', actualPrice: '321.0', img: './images/fd-ebgs.png', ItemName: 'Michael Kors Bag',
                dealvalue: '10', cashBackName: 'Amazon Cash Back', shareBonus: '1%' },
            { discountPrice: '13.0', actualPrice: '19.0', img: './images/fd-inear-beats.png', ItemName: 'In-Ear Beats By Dre',
                dealvalue: '10', cashBackName: 'eBay Cash Back', shareBonus: '1%' },
        ];
        return Rx_1.Observable.of(missedDeals);
    };
    return MissedDealsService;
}());
exports.MissedDealsService = MissedDealsService;
//# sourceMappingURL=app.service.misseddeals.js.map