import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'tabs',
  templateUrl: './apps.tabs.html',
  styleUrls: ['./app.tabs.scss']

})
export class Tabs {
  selectedTab = 0;

  constructor(private router: Router) { }

  onClickLeftArrow() {
    if (this.selectedTab !== 0) {
      this.selectedTab = this.selectedTab - 1;
      alert('indexChanged' + this.selectedTab);
    }
  }
  onClickRightArrow() {
    this.selectedTab = this.selectedTab + 1;
    alert('indexChanged' + this.selectedTab);
  }

  onChange() {
    if (this.selectedTab == 0) {
      this.router.navigateByUrl('deals');
    }
  }
}
