"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var Tabs = (function () {
    function Tabs(router) {
        this.router = router;
        this.selectedTab = 0;
    }
    Tabs.prototype.onClickLeftArrow = function () {
        if (this.selectedTab !== 0) {
            this.selectedTab = this.selectedTab - 1;
            alert('indexChanged' + this.selectedTab);
        }
    };
    Tabs.prototype.onClickRightArrow = function () {
        this.selectedTab = this.selectedTab + 1;
        alert('indexChanged' + this.selectedTab);
    };
    Tabs.prototype.onChange = function () {
        if (this.selectedTab == 0) {
            this.router.navigateByUrl('deals');
        }
    };
    Tabs = __decorate([
        core_1.Component({
            selector: 'tabs',
            templateUrl: './apps.tabs.html',
            styleUrls: ['./app.tabs.scss']
        }),
        __metadata("design:paramtypes", [router_1.Router])
    ], Tabs);
    return Tabs;
}());
exports.Tabs = Tabs;
//# sourceMappingURL=app.tabs.js.map