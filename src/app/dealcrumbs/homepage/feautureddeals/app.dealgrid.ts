import { Component } from '@angular/core';
import { FeaturedDealsService } from './app.featureddealservice';
import { Router } from '@angular/router';

@Component({
  selector: 'featured-deal-grid',
  templateUrl: './app.dealgrid.html',
  styleUrls: ['./app.dealgrid.scss'],
  providers: [FeaturedDealsService]

})
export class DealGrid {
  featuredDealsService: FeaturedDealsService;

  fdeals: [{}];
  constructor(featuredDealsService: FeaturedDealsService,
    private router: Router) {
    this.featuredDealsService = featuredDealsService;
  }
  ngOnInit() {
    this.featuredDealsService.getFeaturedDeals().subscribe(featuredDeals => { this.fdeals = featuredDeals; });
  }

  onClick(deal: any) {
    this.router.navigate(['productDetail/productId/', deal.productId]);
  }
}

