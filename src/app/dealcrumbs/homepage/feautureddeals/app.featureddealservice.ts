import { Observable } from 'rxjs/Rx';
export class FeaturedDealsService {

    featuredDeals = [{
        productId: '1', discountPrice: '190.0', actualPrice: '321.0', img: './images/fd-beats.png', ItemName: 'Studio Beats By Dre',
        dealvalue: '10', cashBackName: ' Amazon Cash Back', shareBonus: '1%'
    },
    {
        productId: '2', discountPrice: '13.0', actualPrice: '19.0', img: './images/fd-chair.png', ItemName: 'Camping Chair',
        dealvalue: '10', cashBackName: 'REI Cash Back', shareBonus: '1%'
    },
    {
        productId: '3', discountPrice: '10.0', actualPrice: '20.0', img: './images/fd-dell.png', ItemName: 'Dell XPS 5000',
        dealvalue: '10', cashBackName: 'Dell Cash Back', shareBonus: '1%'
    },
    {
        productId: '4', discountPrice: '190.0', actualPrice: '321.0', img: './images/fd-ebgs.png', ItemName: 'Michael Kors Bag',
        dealvalue: '10', cashBackName: 'Amazon Cash Back', shareBonus: '1%'
    },
    {
        productId: '5', discountPrice: '13.0', actualPrice: '19.0', img: './images/fd-inear-beats.png', ItemName: 'In-Ear Beats By Dre',
        dealvalue: '10', cashBackName: 'eBay Cash Back', shareBonus: '1%'
    },
    {
        productId: '6', discountPrice: '10.0', actualPrice: '20.0', img: './images/fd-iphone.png', ItemName: 'iPhone 7 Plus',
        dealvalue: '10', cashBackName: 'Verizon Cash Back', shareBonus: '1%'
    },
    {
        productId: '7', discountPrice: '190.0', actualPrice: '321.0', img: './images/fd-nike.png', ItemName: 'Nike Running',
        dealvalue: '10', cashBackName: 'Sears Cash Back', shareBonus: '1%'
    },
    {
        productId: '8', discountPrice: '13.0', actualPrice: '19.0', img: './images/fd-rollingbag.png', ItemName: 'Rolling Bag',
        dealvalue: '10', cashBackName: 'eBay Cash Back',
        shareBonus: '1%'
    },
    {
        productId: '9', discountPrice: '10.0', actualPrice: '20.0', img: './images/fd-shoes.png', ItemName: 'Nike Running Shoe',
        dealvalue: '10', cashBackName: 'Nike Cash Back', shareBonus: '1%'
    },
    {
        productId: '10', discountPrice: '10.0', actualPrice: '20.0', img: './images/fd-beats.png', ItemName: 'Studio Beats By Dre',
        dealvalue: '10', cashBackName: 'BestBuy Cash Back', shareBonus: '1%'
    },
    {
        productId: '11', discountPrice: '60.0', actualPrice: '30.0', img: './images/fd-chair.png', ItemName: 'Camping Chair',
        dealvalue: '10', cashBackName: 'REI Cash Back', shareBonus: '1%'
    },
    {
        productId: '12', discountPrice: '190.0', actualPrice: '321.0', img: './images/fd-inear-beats.png',
        ItemName: 'In-Ear Beats By Dre', dealvalue: '10', cashBackName: 'eBay Cash Back', shareBonus: '1%'
    },
    {
        productId: '13', discountPrice: '13.0', actualPrice: '19.0', img: './images/fd-rollingbag.png', ItemName: 'Rolling Bag',
        dealvalue: '10', cashBackName: 'REI Cash Back',
        shareBonus: '1%'
    },
    {
        productId: '14', discountPrice: '10.0', actualPrice: '20.0', img: './images/fd-iphone.png', ItemName: 'iPhone 7 Plus',
        dealvalue: '10', cashBackName: 'Verizon Cash Back', shareBonus: '1%'
    },
    {
        productId: '15', discountPrice: '20.0', actualPrice: '30.0', img: './images/fd-dell.png', ItemName: 'Dell XPS 5000',
        dealvalue: '10', cashBackName: 'Dell Cash Back', shareBonus: '1%'
    }
    ];


    getFeaturedDeals(): Observable<any> {
        return Observable.of(this.featuredDeals);
    }

    getFeaturedDealByProductId(productId: string): Observable<any> {
        let deal = this.featuredDeals.find((featuredDeal) => {
            return featuredDeal.productId == productId;
        })
        return Observable.of(deal);
    }
}
