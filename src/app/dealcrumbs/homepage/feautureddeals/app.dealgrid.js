"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_featureddealservice_1 = require("./app.featureddealservice");
var router_1 = require("@angular/router");
var DealGrid = (function () {
    function DealGrid(featuredDealsService, router) {
        this.router = router;
        this.featuredDealsService = featuredDealsService;
    }
    DealGrid.prototype.ngOnInit = function () {
        var _this = this;
        this.featuredDealsService.getFeaturedDeals().subscribe(function (featuredDeals) { _this.fdeals = featuredDeals; });
    };
    DealGrid.prototype.onClick = function (deal) {
        this.router.navigate(['productDetail/productId/', deal.productId]);
    };
    DealGrid = __decorate([
        core_1.Component({
            selector: 'featured-deal-grid',
            templateUrl: './app.dealgrid.html',
            styleUrls: ['./app.dealgrid.scss'],
            providers: [app_featureddealservice_1.FeaturedDealsService]
        }),
        __metadata("design:paramtypes", [app_featureddealservice_1.FeaturedDealsService,
            router_1.Router])
    ], DealGrid);
    return DealGrid;
}());
exports.DealGrid = DealGrid;
//# sourceMappingURL=app.dealgrid.js.map