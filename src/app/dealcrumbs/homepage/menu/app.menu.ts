import { Component } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector:'menu',
  templateUrl:'./app.menu.html',
  styleUrls: ['./app.menu.scss']
})
export class Menu
{
  constructor (private router: Router)
     {
     }

onClick() {
    this.router.navigate(['deals']);
  }
}
