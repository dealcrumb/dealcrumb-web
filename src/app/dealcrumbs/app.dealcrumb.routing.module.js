"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var app_deal_1 = require("./homepage/deals/app.deal");
var product_detail_component_1 = require("./product-detail/product-detail/product-detail.component");
exports.APP_ROUTES = [
    { path: '', redirectTo: 'deals', pathMatch: 'full' },
    { path: 'deals', component: app_deal_1.Deals },
    { path: 'productDetail/productId/:productId', component: product_detail_component_1.ProductDetailComponent }
];
//# sourceMappingURL=app.dealcrumb.routing.module.js.map