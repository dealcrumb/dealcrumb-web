import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Deals } from './homepage/deals/app.deal';
import { ProductDetailComponent } from './product-detail/product-detail/product-detail.component';

export const APP_ROUTES: Routes = [
    { path: '', redirectTo: 'deals', pathMatch: 'full' },
    { path: 'deals', component: Deals },
    { path: 'productDetail/productId/:productId', component: ProductDetailComponent }
];