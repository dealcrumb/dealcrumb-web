import { Component, Input } from '@angular/core';

@Component({
    selector: 'product-info-detail-info',
    templateUrl: './product-info-detail-info.component.html',
    styleUrls: ['./product-info-detail-info.component.css']
})
export class ProductInfoDetailInfoComponent {
    @Input() deal: any;
}
