"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ProductInfoPricingHistoryComponent = (function () {
    function ProductInfoPricingHistoryComponent() {
        this.pricingHistory = [
            {
                "name": "Pricing",
                "series": [
                    {
                        "name": "12/16",
                        "value": 198
                    },
                    {
                        "name": "1/17",
                        "value": 165
                    },
                    {
                        "name": "2/17",
                        "value": 165
                    },
                    {
                        "name": "3/17",
                        "value": 140
                    }
                ]
            }
        ];
        this.view = [275, 180];
        // options
        this.showXAxis = true;
        this.showYAxis = true;
        this.gradient = false;
        this.showLegend = false;
        this.showXAxisLabel = false;
        this.showYAxisLabel = false;
        this.colorScheme = {
            domain: ['skyblue']
        };
        // line, area
        this.autoScale = true;
    }
    ;
    ProductInfoPricingHistoryComponent.prototype.onSelect = function (event) {
        console.log(event);
    };
    ProductInfoPricingHistoryComponent = __decorate([
        core_1.Component({
            selector: 'product-info-pricing-history',
            templateUrl: './product-info-pricing-history.component.html',
            styleUrls: ['./product-info-pricing-history.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], ProductInfoPricingHistoryComponent);
    return ProductInfoPricingHistoryComponent;
}());
exports.ProductInfoPricingHistoryComponent = ProductInfoPricingHistoryComponent;
//# sourceMappingURL=product-info-pricing-history.component.js.map