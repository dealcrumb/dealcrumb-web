import { Component } from '@angular/core';

@Component({
    selector: 'product-info-pricing-history',
    templateUrl: './product-info-pricing-history.component.html',
    styleUrls: ['./product-info-pricing-history.component.css']
})
export class ProductInfoPricingHistoryComponent {
    pricingHistory: any[] = [
        {
            "name": "Pricing",
            "series": [
                {
                    "name": "12/16",
                    "value": 198
                },
                {
                    "name": "1/17",
                    "value": 165
                },
                {
                    "name": "2/17",
                    "value": 165
                },
                {
                    "name": "3/17",
                    "value": 140
                }
            ]
        }
    ];;

    view: any[] = [275, 180];

    // options
    showXAxis = true;
    showYAxis = true;
    gradient = false;
    showLegend = false;
    showXAxisLabel = false;
    showYAxisLabel = false;

    colorScheme = {
        domain: ['skyblue']
    };

    // line, area
    autoScale = true;

    constructor() { }

    onSelect(event: any) {
        console.log(event);
    }
}