import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FeaturedDealsService } from '../../homepage/feautureddeals/app.featureddealservice';

@Component({
    selector: 'product-detail',
    templateUrl: './product-detail.component.html',
    styleUrls: ['./product-detail.component.css'],
    providers: [FeaturedDealsService]
})
export class ProductDetailComponent {

    deal: any;

    constructor(private router: ActivatedRoute,
        private featuredDealsService: FeaturedDealsService) { }

    ngOnInit() {
        this.router.params.subscribe(params => {
            if (params['productId']) {
                this.fetchDeal(params['productId']);
            }
        });
    }

    fetchDeal(productId: string) {
        this.featuredDealsService.getFeaturedDealByProductId(productId).subscribe(deal => {
            this.deal = deal;
        })
    }
}