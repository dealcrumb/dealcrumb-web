"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ProductInfoCouponsOtherDealsComponent = (function () {
    function ProductInfoCouponsOtherDealsComponent() {
        this.couponsOtherDeals = [];
    }
    ProductInfoCouponsOtherDealsComponent.prototype.ngOnInit = function () {
        this.couponsOtherDeals = [{
                'date': '2017.05.29',
                'deal': '20% off Apple products'
            }, {
                'date': '2017.05.29',
                'deal': '20% off Apple products'
            }, {
                'date': '2017.05.29',
                'deal': '20% off Apple products'
            }, {
                'date': '2017.05.29',
                'deal': '20% off Apple products'
            }];
    };
    __decorate([
        core_1.Input(),
        __metadata("design:type", Object)
    ], ProductInfoCouponsOtherDealsComponent.prototype, "couponsOtherDeals", void 0);
    ProductInfoCouponsOtherDealsComponent = __decorate([
        core_1.Component({
            selector: 'product-info-coupons-other-deals',
            templateUrl: './product-info-coupons-other-deals.component.html',
            styleUrls: ['./product-info-coupons-other-deals.component.css']
        }),
        __metadata("design:paramtypes", [])
    ], ProductInfoCouponsOtherDealsComponent);
    return ProductInfoCouponsOtherDealsComponent;
}());
exports.ProductInfoCouponsOtherDealsComponent = ProductInfoCouponsOtherDealsComponent;
//# sourceMappingURL=product-info-coupons-other-deals.component.js.map