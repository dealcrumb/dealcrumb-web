import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'product-info-coupons-other-deals',
    templateUrl: './product-info-coupons-other-deals.component.html',
    styleUrls: ['./product-info-coupons-other-deals.component.css']
})
export class ProductInfoCouponsOtherDealsComponent {

    @Input() couponsOtherDeals: any = [];

    constructor() { }

    ngOnInit() {
        this.couponsOtherDeals = [{
            'date': '2017.05.29',
            'deal': '20% off Apple products'
        }, {
            'date': '2017.05.29',
            'deal': '20% off Apple products'
        }, {
            'date': '2017.05.29',
            'deal': '20% off Apple products'
        }, {
            'date': '2017.05.29',
            'deal': '20% off Apple products'
        }]
    }
}