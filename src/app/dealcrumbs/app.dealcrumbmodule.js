"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_homepage_1 = require("./homepage/app.homepage");
var homepage_header_1 = require("./homepage/header/homepage.header");
var material_1 = require("@angular/material");
var app_tabs_1 = require("./homepage/tabs/app.tabs");
var app_deal_1 = require("./homepage/deals/app.deal");
var app_menu_1 = require("./homepage/menu/app.menu");
var app_retailpartners_1 = require("./homepage/stores/app.retailpartners");
var common_1 = require("@angular/common");
var app_banner_1 = require("./homepage/banner/app.banner");
var app_dealgrid_1 = require("./homepage/feautureddeals/app.dealgrid");
var app_featuredeals_footer_1 = require("./homepage/feautureddeals/app.featuredeals.footer");
var app_misseddeals_1 = require("./homepage/misseddeals/app.misseddeals");
var product_detail_component_1 = require("./product-detail/product-detail/product-detail.component");
var product_info_detail_info_component_1 = require("./product-detail/product-info-detail-info/product-info-detail-info.component");
var product_info_pricing_history_component_1 = require("./product-detail/product-info-pricing-history/product-info-pricing-history.component");
var product_info_coupons_other_deals_component_1 = require("./product-detail/product-info-coupons-other-deals/product-info-coupons-other-deals.component");
var app_dealcrumb_routing_module_1 = require("./app.dealcrumb.routing.module");
var router_1 = require("@angular/router");
var ngx_charts_1 = require("@swimlane/ngx-charts");
var DealCrumpModule = (function () {
    function DealCrumpModule() {
    }
    DealCrumpModule = __decorate([
        core_1.NgModule({
            imports: [material_1.MaterialModule, common_1.CommonModule, router_1.RouterModule.forRoot(app_dealcrumb_routing_module_1.APP_ROUTES), ngx_charts_1.NgxChartsModule],
            exports: [app_homepage_1.DealCrumpHome, homepage_header_1.Header, app_tabs_1.Tabs, app_deal_1.Deals, app_menu_1.Menu, app_retailpartners_1.RetailPartners, app_banner_1.Banner, app_dealgrid_1.DealGrid, app_featuredeals_footer_1.FdealsFooter, app_misseddeals_1.MissedDeals, product_detail_component_1.ProductDetailComponent,
                product_info_detail_info_component_1.ProductInfoDetailInfoComponent, product_info_pricing_history_component_1.ProductInfoPricingHistoryComponent, product_info_coupons_other_deals_component_1.ProductInfoCouponsOtherDealsComponent],
            declarations: [app_homepage_1.DealCrumpHome, homepage_header_1.Header, app_tabs_1.Tabs, app_deal_1.Deals, app_menu_1.Menu, app_retailpartners_1.RetailPartners, app_banner_1.Banner, app_dealgrid_1.DealGrid, app_featuredeals_footer_1.FdealsFooter, app_misseddeals_1.MissedDeals, product_detail_component_1.ProductDetailComponent,
                product_info_detail_info_component_1.ProductInfoDetailInfoComponent, product_info_pricing_history_component_1.ProductInfoPricingHistoryComponent, product_info_coupons_other_deals_component_1.ProductInfoCouponsOtherDealsComponent],
            providers: [],
            entryComponents: [],
            schemas: [core_1.CUSTOM_ELEMENTS_SCHEMA]
        })
    ], DealCrumpModule);
    return DealCrumpModule;
}());
exports.DealCrumpModule = DealCrumpModule;
//# sourceMappingURL=app.dealcrumbmodule.js.map