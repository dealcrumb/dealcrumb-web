"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_homepage_1 = require("./homepage/app.homepage");
var homepage_header_1 = require("./homepage/header/homepage.header");
var material_1 = require("@angular/material");
var app_tabs_1 = require("./homepage/tabs/app.tabs");
var app_deal_1 = require("./homepage/deals/app.deal");
var app_menu_1 = require("./homepage/menu/app.menu");
var app_retailpartners_1 = require("./homepage/stores/app.retailpartners");
var DealCrumpModule = (function () {
    function DealCrumpModule() {
    }
    return DealCrumpModule;
}());
DealCrumpModule = __decorate([
    core_1.NgModule({
        imports: [material_1.MaterialModule],
        exports: [app_homepage_1.DealCrumpHome, homepage_header_1.Header, app_tabs_1.Tabs, app_deal_1.Deals, app_menu_1.Menu, app_retailpartners_1.RetailPartners],
        declarations: [app_homepage_1.DealCrumpHome, homepage_header_1.Header, app_tabs_1.Tabs, app_deal_1.Deals, app_menu_1.Menu, app_retailpartners_1.RetailPartners],
        providers: [],
        entryComponents: []
    })
], DealCrumpModule);
exports.DealCrumpModule = DealCrumpModule;
//# sourceMappingURL=app.dealcrumpmodule.js.map