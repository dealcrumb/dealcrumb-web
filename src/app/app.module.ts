import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { DealCrumpModule } from './dealcrumbs/app.dealcrumbmodule';

import { RouterModule } from '@angular/router';
import { APP_ROUTES } from './dealcrumbs/app.dealcrumb.routing.module';

@NgModule({
  imports: [BrowserModule, BrowserAnimationsModule, DealCrumpModule],
  declarations: [AppComponent],
  exports: [AppComponent],
  entryComponents: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
